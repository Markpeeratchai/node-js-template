const express = require('express');
const app = express();
const api = require('./route/index');
const { authenticateToken } = require('./authentication')
const { requestAccessToken } = require('./authentication')
var cors = require('cors')

app.use(express.urlencoded({ limit: '50mb' }));
app.use(cors())

// CORS Config
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS,GET,PUT,POST,DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
app.use(express.json({ limit: '50mb' }))    // <==== parse request body as JSON

// JWT 
// authenticateToken is middle ware to checking authorization
app.use('/api', authenticateToken, api)
app.use('/requestAccessToken', requestAccessToken)

// Error Handler
app.use((err, req, res, next) => {
    let statusCode = err.status || 500
    res.status(statusCode)
    res.json({
        error: {
            status: statusCode,
            message: err.message
        }
    });
});

//start server
app.listen(process.env.PORT, () => {
    console.log(`server running on port ${process.env.PORT}`);
})
