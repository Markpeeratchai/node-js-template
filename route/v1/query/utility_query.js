// config for your database
const { poolPromise } = require('../../../db_')

module.exports = {
    query: async (query) => {
        const pool = await poolPromise
        try {
            const result = await pool.request().query(query);
            return (result.recordset)
        } catch (err) {
            console.error(err);
            throw err
        }
    },
}