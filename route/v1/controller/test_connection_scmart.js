var { query } = require('../query/utility_query')

module.exports = {
    testConnection: async (req, res) => {
        try {
            let result = await query('SELECT ....... FROM .......')

            // can calculate with re before 

            res.status(200).send(result).end()
        } catch (err) {
            // if receive error from query or connection,it will response status 400
            res.status(400).send(err).end()
        }
    }
}