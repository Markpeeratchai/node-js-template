const express = require('express');
const router = express.Router();
var test_connection_art = require('../controller/test_connection_art')
var errorHandle = require('../../../util/errorHandle');

router.get('/test_connection_database', test_connection_art.testConnection, errorHandle.errorHandle);

module.exports = router;
