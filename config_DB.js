const dotenv = require("dotenv");
dotenv.config();

const {
    DB_SERVER,
    DB_NAME,
    DB_USER,
    DB_PWD
} = process.env;

module.exports = {
    port: 1433,
    server: DB_SERVER,
    database: DB_NAME,
    user: DB_USER,
    password: DB_PWD,
    reuseConnection: true,
    stream: true,
    requestTimeout: 600000,
    connectionTimeout: 600000,
    options: {
        encrypt: true,
        keepAlive: true,
        enableArithAbort: true
    },
};
